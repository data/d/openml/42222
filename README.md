# OpenML dataset: Suicide_Dataset

https://www.openml.org/d/42222

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This compiled dataset pulled from four other datasets linked by time and place,and was built to find signals correlated to increased suicide rates among different cohorts globally, across the socio-economic spectrum.

## Contributing

This is a read-only mirror of an [OpenML dataset](https://www.openml.org/d/42222). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42222/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42222/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42222/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

